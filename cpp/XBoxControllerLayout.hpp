
#pragma once

#include "Layout.hpp"




namespace Flat {
namespace Bind {




class FLAT_BIND_EXPORT XBoxControllerLayout : public Layout
{
public:
	enum class Button
	{
		Null        = 0,

		A           = 1,
		B           = 2,
		X           = 3,
		Y           = 4,
		LeftBumper  = 5,
		RightBumper = 6,
		Back        = 7,
		Start       = 8,
		LeftStick   = 9,
		RightStick  = 10,
		Mode        = 11,

		ButtonCount
	};

	enum class Axis
	{
		Null   = 0,

		LeftX  = 1,
		LeftY  = 2,
		RightX = 3,
		RightY = 4,
		LeftZ  = 5,
		RightZ = 6,
		PovX   = 7,
		PovY   = 8,

		AxisCount
	};

	XBoxControllerLayout();
	~XBoxControllerLayout();

	StringRef buttonToString(int button) const override;
	StringRef axisToString(int axisId) const override;

private:
	struct D;
};




}}
