
#include "Handler.hpp"
#include "Handler_p.hpp"

#include <algorithm>
#include <unordered_set>

#include "Debug.hpp"
#include "Context.hpp"
#include "Context_p.hpp"
#include "Device.hpp"
#include "Layout.hpp"




namespace Flat {
namespace Bind {




CommandId Handler::CommandCreator::createCommand()
{
	auto & d = Handler::D::fromCommandCreator(*this);
	const CommandId commandId = d.createCommand();
	return commandId;
}


CommandId Handler::CommandCreator::createToggleCommand()
{
	auto & d = Handler::D::fromCommandCreator(*this);
	const CommandId commandId = d.createCommand();
	auto & commandInfo = d.commandInfoForId[size_t(commandId)];
	commandInfo.isToggle = true;
	return commandId;
}


CommandId Handler::CommandCreator::createAxisCommand()
{
	auto & d = Handler::D::fromCommandCreator(*this);
	const CommandId commandId = d.createCommand();
	auto & commandInfo = d.commandInfoForId[size_t(commandId)];
	commandInfo.isAxis = true;
	return commandId;
}




Context & Handler::ContextCreator::createContext()
{
	auto & d = Handler::D::fromContextCreator(*this);
	return d.createContext();
}




Handler::D::D(Handler & handler, Callbacks & callbacks,
		Input::EventDispatcher & eventDispatcher) noexcept :
	handler(handler),
	callbacks(callbacks),
	eventDispatcher(eventDispatcher)
{
}


Handler::D::~D()
{
	FLAT_ASSERT(lockedContexts.isEmpty());

	while (!actionIdGenerator.isEmpty()) {
		_removeAction(ActionId(actionIdGenerator.first()));
	}

	FLAT_ASSERT(actionIdGenerator.isEmpty());
	FLAT_ASSERT(deviceIdGenerator.isEmpty());
}


CommandId Handler::D::createCommand()
{
	const auto commandId = CommandId(commandInfoForId.size());
	const size_t commandIndex = size_t(commandId);

	if (commandInfoForId.size() <= commandIndex) {
		if (commandInfoForId.capacity() <= commandIndex) {
			commandInfoForId.reserve(commandIndex + 16);
		}
	}
	commandInfoForId.resize(commandIndex + 1);

	CommandInfo & commandInfo = commandInfoForId[commandIndex];

	return commandId;
}


Layout & Handler::D::createLayout(const Handler::CreateLayout & createLayout)
{
	const auto layoutId = LayoutId(layoutForId.size());
	const size_t layoutIndex = size_t(layoutId);

	layoutForId.resize(layoutIndex + 1);

	auto layout = createLayout(layoutId);

	Layout & ref = *layout;
	layoutForId[layoutIndex] = std::move(layout);

	return ref;
}


Context & Handler::D::createContext()
{
	const auto contextId = ContextId(contextForId.size());
	const size_t contextIndex = size_t(contextId);

	contextForId.resize(contextIndex + 1);

	std::unique_ptr<Context> context(new Context(contextId, handler));
	context->d_->commandInfos.resize(commandInfoForId.size());

	Context & ref = *context;
	contextForId[contextIndex] = std::move(context);

	return ref;
}


void Handler::D::_removeAction(const ActionId actionId)
{
	auto id = actionIdGenerator.adapt(int(actionId));
}


void Handler::D::_handleButtonAction(const DeviceId deviceId, const ActionId actionId,
		const bool pressed)
{
	ActionInfo & actionInfo = actionInfoForId[size_t(actionId)];

	const CommandInfo & commandInfo = commandInfoForId[size_t(actionInfo.commandId)];

	FLAT_ASSERT(!commandInfo.isAxis);

	ButtonAction & action = std::get<ButtonAction>(actionInfo.action);

	if (action.isPressed == pressed) {
		// pressed state didn't change, ignore
		return;
	}

	action.isPressed = pressed;

	if (commandInfo.isToggle) {
		// handle both press and release states
		callbacks.onToggleCommand(actionInfo.commandId, pressed);

		if (!pressed) {
			_removeAction(actionId);
		}
	} else {
		// handle only press state
		if (pressed) {
			callbacks.onCommand(actionInfo.commandId);
		}

		_removeAction(actionId);
	}
}


bool Handler::D::handleDeviceButton(const DeviceId deviceId, const Button button, const bool pressed)
{
	// find active action
	for (const auto actionIndex : actionIdGenerator) {
		const auto actionId = ActionId(actionIndex);
		ActionInfo & actionInfo = actionInfoForId[actionIndex];

		if (actionInfo.deviceId != deviceId) {
			// action initiated by another device
			continue;
		}

		if (!std::holds_alternative<ButtonAction>(actionInfo.action)) {
			// action initiated by another button
			continue;
		}

		auto & action = std::get<ButtonAction>(actionInfo.action);

		if (action.button != button) {
			// action initiated by another button
			continue;
		}

		_handleButtonAction(deviceId, actionId, pressed);

		return true;
	}

	// no existing action found, try to find binding for this button
	for (const auto id : lockedContexts) {
		const auto contextId = ContextId(id);
		Context & context = *contextForId[id];

		const auto commandIt = context.d_->commandIdForButton.find(button);
		if (commandIt == context.d_->commandIdForButton.cend()) continue;

		const CommandId commandId = commandIt->second;
		const CommandInfo & commandInfo = commandInfoForId[size_t(commandId)];

		[&] {
			if (commandInfo.isAxis) {
				// TODO: Implement button directions
			} else {
				// find existing action for this command
				for (const auto actionIndex : actionIdGenerator) {
					const auto actionId = ActionId(actionIndex);
					ActionInfo & actionInfo = actionInfoForId[actionIndex];
					if (actionInfo.commandId == commandId) {
						// some action is already executing this command, ignore
						return;
					}
				}

				// create action
				const auto actionId = [&] () -> ActionId {
					auto id = actionIdGenerator.takeUnique();
					const auto actionId = ActionId(id->id());

					if (actionInfoForId.size() <= id->id()) {
						actionInfoForId.resize(id->id() + 1);
					}

					ActionInfo & actionInfo = actionInfoForId[id->id()];
					actionInfo.deviceId = deviceId;
					actionInfo.contextId = contextId;
					actionInfo.commandId = commandId;

					ButtonAction action;
					action.button = button;
					actionInfo.action = action;

					id.release();

					return ActionId(actionId);
				}();

				_handleButtonAction(deviceId, actionId, pressed);
			}
		}();

		return true;
	}

	return false;
}


//void handleDeviceAxis(DeviceId deviceId, Axis axis, float value);


void Handler::D::lockContext(Context & context)
{
	lockedContexts.reserve(int(context.d_->contextId));
}


void Handler::D::unlockContext(Context & context)
{
	lockedContexts.free(int(context.d_->contextId));
}


void Handler::D::removeDevice(Device & device)
{
	const int id = int(device.deviceId_);
	auto & devicep = deviceForId[id];
	devicep = nullptr;
	deviceIdGenerator.free(id);
}




Handler::Handler(Callbacks & callback,
		const CreateCommandsCallback & createCommands,
		const CreateLayoutsCallback & createLayouts,
		const CreateContextsCallback & createContexts,
		Input::EventDispatcher & eventDispatcher) :
	d_(*new D(*this, callback, eventDispatcher))
{
	createCommands(reinterpret_cast<CommandCreator&>(*d_));
	createLayouts(reinterpret_cast<LayoutCreator&>(*d_));
	createContexts(reinterpret_cast<ContextCreator&>(*d_));
}


Handler::~Handler()
{
}


Handler & Handler::_fromLayoutCreator(LayoutCreator & c) noexcept
{
	return D::fromLayoutCreator(c).handler;
}


std::unique_ptr<Device> Handler::addDevice()
{
	Utils::IdGenerator::Id id = d_->deviceIdGenerator.takeUnique();

	if (d_->deviceForId.size() <= id->id()) {
		d_->deviceForId.resize(id->id() + 1);
	}

	const auto deviceId = DeviceId(id->id());

	std::unique_ptr<Device> device(new Device(*this, deviceId));

	d_->deviceForId[id->id()] = device.get();

	id.release();

	return device;
}


#if 0
bool Handler::isCommandToggle(const CommandId commandId) const noexcept
{
	const auto & commandInfo = d_->commandInfoForId.at(size_t(commandId));
	FLAT_ASSERT(commandInfo.commandId != CommandId::Null);
	return commandInfo.isToggle;
}


bool Handler::isCommandAxis(const CommandId commandId) const noexcept
{
	const auto & commandInfo = d_->commandInfoForId.at(size_t(commandId));
	FLAT_ASSERT(commandInfo.commandId != CommandId::Null);
	return commandInfo.isAxis;
}
#endif


#if 0
int Manager::createCommandGroup()
{
	const int groupId = d_->commandGroupIdGenerator.take();

	if ( d_->commandGroupInfoForId.size() <= groupId )
		d_->commandGroupInfoForId.resize( groupId + 1 );

	ManagerPrivate::CommandGroupInfo groupInfo;
	groupInfo.enabled = false;
	d_->commandGroupInfoForId[ groupId ] = groupInfo;

	return groupId;
}


void Manager::addCommandToGroup( int groupId, const QString & command )
{
	FLAT_ASSERT(groupId > 0 && groupId < d_->commandGroupInfoForId.size());
	ManagerPrivate::CommandGroupInfo & groupInfo = d_->commandGroupInfoForId[ groupId ];
	groupInfo.commands << command;
	d_->groupIdForCommand[ command ] = groupId;
}


void Manager::setCommandGroupEnabled( int groupId, bool enabled )
{
	FLAT_ASSERT(groupId > 0 && groupId < d_->commandGroupInfoForId.size());
	d_->commandGroupInfoForId[ groupId ].enabled = enabled;
}
#endif


#if 0
std::vector<Context*> Handler::currentContexts() const
{
	return d_->currentContexts;
}


void Handler::setCurrentContexts(const std::vector<Context*> & contexts)
{
	{
		const std::vector<Context*> & c = d_->contexts;
		std::unordered_set<Context*> set;
		for (const auto context : contexts) {
			FLAT_ASSERT(std::find(c.cbegin(), c.cend(), context) != c.cend());
			FLAT_ASSERT(set.find(context) == set.cend());
			set.insert(context);
		}
	}

	d_->currentContexts = contexts;
}
#endif


Layout & Handler::_createLayout(const CreateLayout & createLayout)
{
	return d_->createLayout(createLayout);
}





}}
