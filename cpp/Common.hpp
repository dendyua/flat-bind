
#pragma once

#include <cstdint>
#include <functional>




namespace Flat {
namespace Bind {





class Layout;




enum class LayoutId : int { Null = -1 };
enum class ButtonId : int {};
enum class AxisId : int {};
enum class CommandId : int { Null = -1 };
enum class DeviceId : int {};
enum class ContextId {};
enum class ActionId {};




class FLAT_BIND_EXPORT Button
{
public:
	Button() noexcept :
		layoutId_(LayoutId::Null)
	{}

	bool operator==(const Button & button) const noexcept {
		if (isNull()) return button.isNull();
		return layoutId_ == button.layoutId_ && buttonId_ == button.buttonId_;
	}

	bool operator!=(const Button & button) const noexcept {
		return !operator==(button);
	}

	bool isNull() const noexcept { return layoutId_ == LayoutId::Null; }

	LayoutId layoutId() const noexcept { return layoutId_; }
	ButtonId buttonId() const { return buttonId_; }

#if 0
	QString toString() const;
	QString name() const;
#endif

private:
	Button(LayoutId layoutId, ButtonId buttonId) noexcept :
		layoutId_(layoutId),
		buttonId_(buttonId)
	{}

private:
	LayoutId layoutId_ = LayoutId::Null;
	ButtonId buttonId_;

	friend class Layout;
};




class FLAT_BIND_EXPORT Axis
{
public:
	Axis() noexcept :
		layoutId_(LayoutId::Null)
	{}

	bool operator==(const Axis & axis) const noexcept {
		if (isNull()) return axis.isNull();
		return layoutId_ == axis.layoutId_ && axisId_ == axis.axisId_;
	}

	bool isNull() const noexcept { return layoutId_ == LayoutId::Null; }

	LayoutId layoutId() const noexcept { return layoutId_; }
	AxisId axisId() const noexcept { return axisId_; }

#if 0
	QString toString() const;
	QString name() const;
#endif

private:
	Axis(LayoutId layoutId, AxisId axisId) noexcept :
		layoutId_(layoutId),
		axisId_(axisId)
	{}

private:
	LayoutId layoutId_;
	AxisId axisId_;

	friend class Layout;
};




#if 0
class FLAT_BIND_EXPORT AxisDirection
{
public:
	AxisDirection() noexcept : axis_(Axis::Null) {}
	AxisDirection(const Axis & axis, bool forward) noexcept;

	bool operator==( const AxisDirection & other ) const;

	bool isNull() const;

	Axis axis() const;
	bool forward() const;

private:
	Axis axis_;
	bool forward_;
};

inline AxisDirection::AxisDirection() :
	forward_( false )
{}

inline AxisDirection::AxisDirection( const Axis & axis, const bool forward ) :
	axis_( axis ), forward_( forward )
{}

inline bool AxisDirection::operator==( const AxisDirection & other ) const
{ return axis_ == other.axis_ && forward_ == other.forward_; }

inline bool AxisDirection::isNull() const
{ return axis_.isNull(); }

inline Axis AxisDirection::axis() const
{ return axis_; }

inline bool AxisDirection::forward() const
{ return forward_; }

inline uint qHash( const AxisDirection & axis )
{ return Grim::Bind::qHash( axis.axis() ) + ::qHash( axis.forward() ); }
#endif




}}




namespace std {

template <>
struct hash<Flat::Bind::Button> {
	inline size_t operator()(const Flat::Bind::Button & b) const
	{ return std::hash<uint64_t>()((uint64_t(b.layoutId()) << 32) | uint64_t(b.buttonId())); }
};

template <>
struct hash<Flat::Bind::Axis> {
	inline size_t operator()(const Flat::Bind::Axis & a) const
	{ return std::hash<uint64_t>()((uint64_t(a.layoutId()) << 32) | uint64_t(a.axisId())); }
};

}
