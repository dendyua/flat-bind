
#pragma once

#include <memory>
#include <vector>

#include <Flat/Core/Ref.hpp>

#include "Common.hpp"




namespace Flat {
namespace Input {
	class EventDispatcher;
}}




namespace Flat {
namespace Bind {




class Layout;
class Handler;
class Context;
class Device;




struct Callbacks
{
	virtual ~Callbacks() = default;

	virtual void onCommand(CommandId commandId) = 0;
	virtual void onToggleCommand(CommandId commandId, bool on) = 0;
	virtual void onRelativeCommand(CommandId commandId, float value) = 0;
};




class FLAT_BIND_EXPORT Handler
{
public:
	struct CommandCreator
	{
		CommandId createCommand();
		CommandId createToggleCommand();
		CommandId createAxisCommand();
	};

	struct LayoutCreator
	{
		template<typename T>
		Layout & addLayout();
	};

	struct ContextCreator
	{
		Context & createContext();
	};

	typedef std::function<void(CommandCreator&)> CreateCommandsCallback;
	typedef std::function<void(LayoutCreator&)> CreateLayoutsCallback;
	typedef std::function<void(ContextCreator&)> CreateContextsCallback;

	Handler(Callbacks & callbacks,
			const CreateCommandsCallback & createCommands,
			const CreateLayoutsCallback & createLayouts,
			const CreateContextsCallback & createContexts,
			Input::EventDispatcher & eventDispatcher);
	~Handler();

	std::unique_ptr<Device> addDevice();

#if 0
	bool isCommandToggle(CommandId commandId) const noexcept;
	bool isCommandAxis(CommandId commandId) const noexcept;
#endif

#if 0
	int createCommandGroup();
	void addCommandToGroup( int groupId, const QString & command );
	void setCommandGroupEnabled( int groupId, bool enabled );
#endif

private:
	struct D;

	typedef std::function<std::unique_ptr<Layout>(LayoutId)> CreateLayout;

	static Handler & _fromLayoutCreator(LayoutCreator & c) noexcept;

	Layout & _createLayout(const CreateLayout & createLayout);

	Ref<D> d_;

	friend class Layout;
	friend class Context;
	friend class Device;
};




template<typename T>
inline Layout & Handler::LayoutCreator::addLayout()
{
	const CreateLayout create = [] (LayoutId layoutId) -> std::unique_ptr<Layout> {
		return std::unique_ptr<Layout>(new T(layoutId));
	};
	return Handler::_fromLayoutCreator(*this)._createLayout(create);
}




}}
