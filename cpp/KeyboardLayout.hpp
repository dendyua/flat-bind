
#pragma once

#include "Layout.hpp"




namespace Flat {
namespace Bind {




class FLAT_BIND_EXPORT KeyboardLayout : public Layout
{
public:
	enum class Key : int
	{
		Null = 0,

		Space,
		Apostrophe,
		Comma,
		Minus,
		Period,
		Slash,
		Number_0,
		Number_1,
		Number_2,
		Number_3,
		Number_4,
		Number_5,
		Number_6,
		Number_7,
		Number_8,
		Number_9,
		Semicolon,
		Equal,
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		LeftBracket,
		Backslash,
		RightBracket,
		GraveAccent,
		World_1,
		World_2,

		Escape,
		Enter,
		Tab,
		Backspace,
		Insert,
		Delete,
		Right,
		Left,
		Down,
		Up,
		PageUp,
		PageDown,
		Home,
		End,
		CapsLock,
		ScrollLock,
		NumLock,
		PrintScreen,
		Pause,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		F13,
		F14,
		F15,
		F16,
		F17,
		F18,
		F19,
		F20,
		F21,
		F22,
		F23,
		F24,
		F25,
		Keypad_0,
		Keypad_1,
		Keypad_2,
		Keypad_3,
		Keypad_4,
		Keypad_5,
		Keypad_6,
		Keypad_7,
		Keypad_8,
		Keypad_9,
		Keypad_Decimal,
		Keypad_Divide,
		Keypad_Multiply,
		Keypad_Subtract,
		Keypad_Add,
		Keypad_Enter,
		Keypad_Equal,
		LeftShift,
		LeftControl,
		LeftAlt,
		LeftSuper,
		RightShift,
		RightControl,
		RightAlt,
		RightSuper,
		Menu,
	};

	static ButtonId buttonIdForKey(Key key) noexcept { return ButtonId(key); }

	KeyboardLayout(LayoutId layoutId);
	~KeyboardLayout();

#if 0
	StringRef buttonToString(ButtonId buttonId) const override;
	StringRef axisToString(int axisId) const override;
#endif

private:
	struct D;
};




}}
