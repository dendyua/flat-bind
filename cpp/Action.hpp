
#pragma once

#include "Common.hpp"




namespace Flat {
namespace Bind {




enum class ActionType
{
	Button,
	Axis,
};




template <ActionType>
struct BaseAction
{
};




struct ButtonAction : BaseAction<ActionType::Button>
{
	Button button;
	bool isReleasable;
	bool isRepeatable;
	int64_t repeatInterval;

	bool isPressed = false;
};




struct AxisAction : BaseAction<ActionType::Axis>
{
	float value = 0;
};




}}
