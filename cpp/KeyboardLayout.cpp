
#include "KeyboardLayout.hpp"

#include <unordered_map>

#include "Layout_p.hpp"




namespace Flat {
namespace Bind {




struct KeyboardLayout::D : public Layout::D
{
	D(LayoutId layoutId) noexcept :
		Layout::D(layoutId)
	{}

#if 0
	void addButton( int button, const QString & string, const char * translationName );

	std::unordered_map<std::string, int> buttonIdForString;
	std::unordered_map<int, ButtonInfo> buttonInfoForButtonId;
#endif
};




#if 0
void KeyboardDevicePrivate::addButton( const int button, const QString & string, const char * const translationName )
{
	buttonForString_[ string ] = button;

	ButtonInfo buttonInfo;
	buttonInfo.button = button;
	buttonInfo.string = string;
	buttonInfo.translationName = translationName;
	buttonInfoForButton_[ button ] = buttonInfo;
}
#endif




KeyboardLayout::KeyboardLayout(const LayoutId layoutId) :
	Layout(*new D(layoutId))
{
#if 0
	d->addButton( Qt::Key_Escape, QLatin1String( "escape" ), QT_TR_NOOP("Escape") );
	d->addButton( Qt::Key_Tab,    QLatin1String( "tab" ),    QT_TR_NOOP("Tab") );
	d->addButton( Qt::Key_F1,     QLatin1String( "f1" ),     QT_TR_NOOP("F1") );
	d->addButton( Qt::Key_F2,     QLatin1String( "f2" ),     QT_TR_NOOP("F2") );
	d->addButton( Qt::Key_F3,     QLatin1String( "f3" ),     QT_TR_NOOP("F3") );
	d->addButton( Qt::Key_F4,     QLatin1String( "f4" ),     QT_TR_NOOP("F4") );
	d->addButton( Qt::Key_F5,     QLatin1String( "f5" ),     QT_TR_NOOP("F5") );
	d->addButton( Qt::Key_F6,     QLatin1String( "f6" ),     QT_TR_NOOP("F6") );
	d->addButton( Qt::Key_F7,     QLatin1String( "f7" ),     QT_TR_NOOP("F7") );
	d->addButton( Qt::Key_F8,     QLatin1String( "f8" ),     QT_TR_NOOP("F8") );
	d->addButton( Qt::Key_F9,     QLatin1String( "f9" ),     QT_TR_NOOP("F9") );
	d->addButton( Qt::Key_F10,    QLatin1String( "f10" ),    QT_TR_NOOP("F10") );
	d->addButton( Qt::Key_F11,    QLatin1String( "f11" ),    QT_TR_NOOP("F11") );
	d->addButton( Qt::Key_F12,    QLatin1String( "f12" ),    QT_TR_NOOP("F12") );

	d->addButton( Qt::Key_1,      QLatin1String( "1" ),      QT_TR_NOOP("1") );
	d->addButton( Qt::Key_2,      QLatin1String( "2" ),      QT_TR_NOOP("2") );
	d->addButton( Qt::Key_3,      QLatin1String( "3" ),      QT_TR_NOOP("3") );
	d->addButton( Qt::Key_4,      QLatin1String( "4" ),      QT_TR_NOOP("4") );
	d->addButton( Qt::Key_5,      QLatin1String( "5" ),      QT_TR_NOOP("5") );
	d->addButton( Qt::Key_6,      QLatin1String( "6" ),      QT_TR_NOOP("6") );
	d->addButton( Qt::Key_7,      QLatin1String( "7" ),      QT_TR_NOOP("7") );
	d->addButton( Qt::Key_8,      QLatin1String( "8" ),      QT_TR_NOOP("8") );
	d->addButton( Qt::Key_9,      QLatin1String( "9" ),      QT_TR_NOOP("9") );
	d->addButton( Qt::Key_0,      QLatin1String( "0" ),      QT_TR_NOOP("0") );

	d->addButton( Qt::Key_A,      QLatin1String( "a" ),      QT_TR_NOOP("A") );
	d->addButton( Qt::Key_B,      QLatin1String( "b" ),      QT_TR_NOOP("B") );
	d->addButton( Qt::Key_C,      QLatin1String( "c" ),      QT_TR_NOOP("C") );
	d->addButton( Qt::Key_D,      QLatin1String( "d" ),      QT_TR_NOOP("D") );
	d->addButton( Qt::Key_E,      QLatin1String( "e" ),      QT_TR_NOOP("E") );
	d->addButton( Qt::Key_F,      QLatin1String( "f" ),      QT_TR_NOOP("F") );
	d->addButton( Qt::Key_G,      QLatin1String( "g" ),      QT_TR_NOOP("G") );
	d->addButton( Qt::Key_H,      QLatin1String( "h" ),      QT_TR_NOOP("H") );
	d->addButton( Qt::Key_I,      QLatin1String( "i" ),      QT_TR_NOOP("I") );
	d->addButton( Qt::Key_J,      QLatin1String( "j" ),      QT_TR_NOOP("J") );
	d->addButton( Qt::Key_K,      QLatin1String( "k" ),      QT_TR_NOOP("K") );
	d->addButton( Qt::Key_L,      QLatin1String( "l" ),      QT_TR_NOOP("L") );
	d->addButton( Qt::Key_M,      QLatin1String( "m" ),      QT_TR_NOOP("M") );
	d->addButton( Qt::Key_N,      QLatin1String( "n" ),      QT_TR_NOOP("N") );
	d->addButton( Qt::Key_O,      QLatin1String( "o" ),      QT_TR_NOOP("O") );
	d->addButton( Qt::Key_P,      QLatin1String( "p" ),      QT_TR_NOOP("P") );
	d->addButton( Qt::Key_Q,      QLatin1String( "q" ),      QT_TR_NOOP("Q") );
	d->addButton( Qt::Key_R,      QLatin1String( "r" ),      QT_TR_NOOP("R") );
	d->addButton( Qt::Key_S,      QLatin1String( "s" ),      QT_TR_NOOP("S") );
	d->addButton( Qt::Key_T,      QLatin1String( "t" ),      QT_TR_NOOP("T") );
	d->addButton( Qt::Key_U,      QLatin1String( "u" ),      QT_TR_NOOP("U") );
	d->addButton( Qt::Key_V,      QLatin1String( "v" ),      QT_TR_NOOP("V") );
	d->addButton( Qt::Key_W,      QLatin1String( "w" ),      QT_TR_NOOP("W") );
	d->addButton( Qt::Key_X,      QLatin1String( "x" ),      QT_TR_NOOP("X") );
	d->addButton( Qt::Key_Y,      QLatin1String( "y" ),      QT_TR_NOOP("Y") );
	d->addButton( Qt::Key_Z,      QLatin1String( "z" ),      QT_TR_NOOP("Z") );

	d->addButton( Qt::Key_Space,  QLatin1String( "space" ),  QT_TR_NOOP("Space") );
	d->addButton( Qt::Key_Left,   QLatin1String( "left" ),   QT_TR_NOOP("Left") );
	d->addButton( Qt::Key_Right,  QLatin1String( "right" ),  QT_TR_NOOP("Right") );
	d->addButton( Qt::Key_Up,     QLatin1String( "up" ),     QT_TR_NOOP("Up") );
	d->addButton( Qt::Key_Down,   QLatin1String( "down" ),   QT_TR_NOOP("Down") );
#endif
}


KeyboardLayout::~KeyboardLayout()
{
}


#if 0
QString KeyboardDevice::buttonToString( const int button ) const
{
	return constData()->buttonInfoForButton_.value( button ).string;
}


QString KeyboardDevice::buttonName( const int button ) const
{
	return tr( constData()->buttonInfoForButton_.value( button ).translationName );
}


QString KeyboardDevice::axisToString( const int axis ) const
{
	FLAT_UNUSED(axis)
	return QString();
}


QString KeyboardDevice::axisName( const int axis ) const
{
	FLAT_UNUSED(axis)
	return QString();
}
#endif




}}
