
#include "XBoxControllerLayout.hpp"

#include "Layout_p.hpp"




namespace Flat {
namespace Bind {




struct XBoxControllerLayout::D : public Layout::D
{
	void addButton(const int buttonId, const QString & string, const char * const translationName );
	void addAxis( const int axis, const QString & name, const char * const translationName );

	QHash<QString,int> buttonForString;
	QVector<ButtonInfo> buttonInfoForButton;

	QHash<QString,int> axisForString;
	QVector<AxisInfo> axisInfoForAxis;
};




void XBoxControllerDevicePrivate::addButton( const int button, const QString & string, const char * const translationName )
{
	buttonForString[ string ] = button;

	ButtonInfo buttonInfo;
	buttonInfo.button = button;
	buttonInfo.string = string;
	buttonInfo.translationName = translationName;
	buttonInfoForButton[ button ] = buttonInfo;
}


void XBoxControllerDevicePrivate::addAxis( const int axis, const QString & string, const char * const translationName )
{
	axisForString[ string ] = axis;

	AxisInfo axisInfo;
	axisInfo.axis = axis;
	axisInfo.string = string;
	axisInfo.translationName = translationName;
	axisInfoForAxis[ axis ] = axisInfo;
}




XBoxControllerDevice::ButtonType XBoxControllerDevice::buttonFromGamepadButton( const Gamepad::ButtonType buttonType )
{
	switch ( buttonType )
	{
	case Gamepad::Button_A:      return Button_A;
	case Gamepad::Button_B:      return Button_B;
	case Gamepad::Button_X:      return Button_X;
	case Gamepad::Button_Y:      return Button_Y;
	case Gamepad::Button_TL:     return Button_LeftBumper;
	case Gamepad::Button_TR:     return Button_RightBumper;
	case Gamepad::Button_Select: return Button_Back;
	case Gamepad::Button_Start:  return Button_Start;
	case Gamepad::Button_Mode:   return Button_Mode;
	case Gamepad::Button_ThumbL: return Button_LeftStick;
	case Gamepad::Button_ThumbR: return Button_RightStick;
	default: break;
	}

	return Button_Null;
}


XBoxControllerDevice::AxisType XBoxControllerDevice::axisFromGamepadAxis( const Gamepad::AxisType axisType )
{
	switch ( axisType )
	{
	case Gamepad::Axis_X:    return Axis_LeftX;
	case Gamepad::Axis_Y:    return Axis_LeftY;
	case Gamepad::Axis_Z:    return Axis_LeftZ;
	case Gamepad::Axis_RX:   return Axis_RightX;
	case Gamepad::Axis_RY:   return Axis_RightY;
	case Gamepad::Axis_RZ:   return Axis_RightZ;
	case Gamepad::Axis_PovX: return Axis_PovX;
	case Gamepad::Axis_PovY: return Axis_PovY;
	default: break;
	}

	return Axis_Null;
}




inline const XBoxControllerDevicePrivate * XBoxControllerDevice::constData() const
{ return constCastTo<XBoxControllerDevicePrivate>(); }

inline XBoxControllerDevicePrivate * XBoxControllerDevice::data() const
{ return castTo<XBoxControllerDevicePrivate>(); }




XBoxControllerDevice::XBoxControllerDevice() :
	Device( new XBoxControllerDevicePrivate )
{
	XBoxControllerDevicePrivate * const d = data();

	d->buttonInfoForButton.resize( Button_TotalButtons );
	d->addButton( Button_A,           QLatin1String( "a" ),      QT_TR_NOOP("A") );
	d->addButton( Button_B,           QLatin1String( "b" ),      QT_TR_NOOP("B") );
	d->addButton( Button_X,           QLatin1String( "x" ),      QT_TR_NOOP("X") );
	d->addButton( Button_Y,           QLatin1String( "y" ),      QT_TR_NOOP("Y") );
	d->addButton( Button_LeftBumper,  QLatin1String( "lshift" ), QT_TR_NOOP("Left Shift") );
	d->addButton( Button_RightBumper, QLatin1String( "rshift" ), QT_TR_NOOP("Right Shift") );
	d->addButton( Button_Back,        QLatin1String( "back" ),   QT_TR_NOOP("Back") );
	d->addButton( Button_Start,       QLatin1String( "start" ),  QT_TR_NOOP("Start") );
	d->addButton( Button_LeftStick,   QLatin1String( "lstick" ), QT_TR_NOOP("Left Stick") );
	d->addButton( Button_RightStick,  QLatin1String( "rstick" ), QT_TR_NOOP("Right Stick") );

	d->axisInfoForAxis.resize( Axis_TotalAxes );
	d->addAxis( Axis_LeftX,  QLatin1String( "leftx" ),  QT_TR_NOOP("Left X") );
	d->addAxis( Axis_LeftY,  QLatin1String( "lefty" ),  QT_TR_NOOP("Left Y") );
	d->addAxis( Axis_RightX, QLatin1String( "rightx" ), QT_TR_NOOP("Right X") );
	d->addAxis( Axis_RightY, QLatin1String( "righty" ), QT_TR_NOOP("Right Y") );
	d->addAxis( Axis_LeftZ,  QLatin1String( "leftz" ),  QT_TR_NOOP("Left Trigger") );
	d->addAxis( Axis_RightZ, QLatin1String( "rightz" ), QT_TR_NOOP("Right Trigger") );
	d->addAxis( Axis_PovX,   QLatin1String( "povx" ),   QT_TR_NOOP("Point of view X") );
	d->addAxis( Axis_PovY,   QLatin1String( "povy" ),   QT_TR_NOOP("Point of view Y") );
}


XBoxControllerDevice::~XBoxControllerDevice()
{
}


QString XBoxControllerDevice::buttonToString( const int button ) const
{
	return constData()->buttonInfoForButton.at( button ).string;
}


QString XBoxControllerDevice::buttonName( const int button ) const
{
	return tr( constData()->buttonInfoForButton.at( button ).translationName );
}


QString XBoxControllerDevice::axisToString( const int axis ) const
{
	return constData()->axisInfoForAxis.at( axis ).string;
}


QString XBoxControllerDevice::axisName( const int axis ) const
{
	return tr( constData()->axisInfoForAxis.at( axis ).translationName );
}




}}
