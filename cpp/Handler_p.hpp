
#pragma once

#include <vector>
#include <variant>

#include <Flat/Utils/IdGenerator.hpp>
#include <Flat/Input/EventDispatcher.hpp>

#include "Handler.hpp"
#include "Action.hpp"




namespace Flat {
namespace Bind {




struct Handler::D
{
	struct CommandInfo
	{
		bool isToggle = false;
		bool isAxis = false;
	};

	struct ActionInfo
	{
		DeviceId deviceId;
		ContextId contextId;
		CommandId commandId;

		std::variant<ButtonAction> action;
	};

	D(Handler & handler, Callbacks & callbacks,
			Input::EventDispatcher & eventDispatcher) noexcept;
	~D();

	inline static D & fromCommandCreator(Handler::CommandCreator & c) noexcept
	{ return reinterpret_cast<D&>(c); }

	inline static D & fromLayoutCreator(Handler::LayoutCreator & c) noexcept
	{ return reinterpret_cast<D&>(c); }

	inline static D & fromContextCreator(Handler::ContextCreator & c) noexcept
	{ return reinterpret_cast<D&>(c); }

	bool handleDeviceButton(DeviceId deviceId, Button button, bool pressed);
	bool handleDeviceAxis(DeviceId deviceId, Axis axis, float value);

	CommandId createCommand();

	Layout & createLayout(const Handler::CreateLayout & createLayout);

	Context & createContext();

	void lockContext(Context & context);
	void unlockContext(Context & context);

	void removeDevice(Device & device);

	Handler & handler;
	Callbacks & callbacks;
	Input::EventDispatcher & eventDispatcher;

	std::vector<CommandInfo> commandInfoForId;

	std::vector<std::unique_ptr<Layout>> layoutForId;

	Utils::IdGenerator lockedContexts;
	std::vector<std::unique_ptr<Context>> contextForId;

	Utils::IdGenerator deviceIdGenerator;
	std::vector<Device*> deviceForId;

	Utils::IdGenerator actionIdGenerator;
	std::vector<ActionInfo> actionInfoForId;

#if 0
	class CommandGroupInfo
	{
	public:
		CommandGroupInfo() :
			enabled( false )
		{}

		QStringList commands;
		bool enabled;
	};

	Tools::IdGenerator commandGroupIdGenerator;
	QVector<CommandGroupInfo> commandGroupInfoForId;
	QHash<QString,int> groupIdForCommand;
#endif

private:
	void _removeAction(ActionId actionId);

	void _handleButtonAction(DeviceId deviceId, ActionId actionId, bool pressed);
//	void _handleAxisCommand(DeviceId deviceId, CommandInfo & commandInfo, float value);
};




}}
