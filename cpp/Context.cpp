
#include "Context.hpp"
#include "Context_p.hpp"

#include <algorithm>
#include <vector>
#include <unordered_map>

#include "Debug.hpp"
#include "Handler.hpp"
#include "Handler_p.hpp"




namespace Flat {
namespace Bind {




void Context::LockDeleter::operator()(LockData * l) noexcept
{
	Context::D & d = *reinterpret_cast<Context*>(l)->d_;
	d.unlock();
}




void Context::D::lock()
{
	lockCount_++;
	if (lockCount_ == 1) {
		handler.d_->lockContext(context);
	}
}


void Context::D::unlock()
{
	FLAT_ASSERT(lockCount_ > 0);
	lockCount_--;
	if (lockCount_ == 0) {
		handler.d_->unlockContext(context);
	}
}


CommandId Context::D::detachButton(const Button button) noexcept
{
	const auto it = commandIdForButton.find(button);
	if (it == commandIdForButton.end()) return CommandId::Null;

	const CommandId commandId = it->second;
	commandIdForButton.erase(it);

	CommandInfo & commandInfo = commandInfos[size_t(commandId)];
	const auto buttonIt = std::find(commandInfo.boundButtons.begin(),
			commandInfo.boundButtons.end(), button);
	FLAT_ASSERT(buttonIt != commandInfo.boundButtons.cend());
	commandInfo.boundButtons.erase(buttonIt);

	return commandId;
}


#if 0
CommandId Context::D::detachAxis(const Axis & axis) noexcept
{
	const auto it = std::find(commandIdForAxis.begin(), commandIdForAxis.end(), axis);
	if (it == commandIdForAxis.end()) return CommandId::Null;

	const CommandId commandId = it->second;
	commandIdForAxis.erase(it);

	CommandInfo & commandInfo = commandInfos[size_t(commandId)];
	const auto axisIt = std::find(commandInfo.boundAxes.begin(), commandInfo.boundAxes.end(), axis);
	FLAT_ASSERT(axisIt != commandInfo.boundAxes.end());
	commandInfo.boundAxes.erase(axisIt);

	return commandId;
}
#endif


#if 0
int ContextPrivate::detachAxisDirection( const AxisDirection & axisDirection )
{
	const int command = commandForAxisDirection.take( axisDirection );

	if ( command != 0 )
	{
		CommandInfo & commandInfo = commandInfos[ command ];
		FLAT_ASSERT(commandInfo.boundAxisDirections.count( axisDirection ) == 1);
		commandInfo.boundAxisDirections.removeOne( axisDirection );
	}

	return command;
}
#endif




Context::Context(const ContextId contextId, Handler & handler) noexcept :
	d_(*new D(contextId, *this, handler))
{
}


Context::~Context() noexcept
{
}


Context::Lock Context::lock()
{
	d_->lock();
	return Lock(reinterpret_cast<LockData*>(this), LockDeleter{});
}


void Context::attachButtonToCommand(const Button button, const CommandId commandId)
{
	auto & commandInfo = [this, button, commandId] () -> D::CommandInfo & {
		const auto commandIt = d_->commandIdForButton.find(button);
		if (commandIt != d_->commandIdForButton.cend()) {
			// unbind previous command
			const CommandId previousCommandId = commandIt->second;

			// TODO: emit unbind?

			D::CommandInfo & previousCommandInfo = d_->commandInfos[int(previousCommandId)];
			const auto it = std::find(previousCommandInfo.boundButtons.begin(),
					previousCommandInfo.boundButtons.end(), button);
			FLAT_ASSERT(it != previousCommandInfo.boundButtons.cend());
			previousCommandInfo.boundButtons.erase(it);
			return previousCommandInfo;
		}

		d_->commandIdForButton[button] = commandId;
		return d_->commandInfos[int(commandId)];
	}();

	commandInfo.boundButtons.push_back(button);

	// TODO: emit bind?
}


#if 0
void Context::attachAxisDirectionToCommand( const AxisDirection & axisDirection, const int command )
{
	if ( d_->commandForAxisDirection.value( axisDirection ) == command )
		return;

	{
		const int previousAxisCommand = d_->detachAxis( axisDirection.axis() );
		if ( previousAxisCommand != 0 )
			emit bindChanged( previousAxisCommand );

		const int previousCommand = d_->detachAxisDirection( axisDirection );
		if ( previousCommand != 0 )
			emit bindChanged( previousCommand );
	}

	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	FLAT_ASSERT(!commandInfo.isNull());

	d_->commandForAxisDirection[ axisDirection ] = command;
	commandInfo.boundAxisDirections << axisDirection;

	emit bindChanged( command );
}


void Context::attachAxisToCommand( const Axis & axis, const int command )
{
	if ( d_->commandForAxis.value( axis ) == command )
		return;

	{
		const int previousCommand = d_->detachAxis( axis );
		if ( previousCommand != 0 )
			emit bindChanged( previousCommand );

		const int previousBackwardCommand = d_->detachAxisDirection( AxisDirection( axis, false ) );
		if ( previousBackwardCommand != 0 )
			emit bindChanged( previousBackwardCommand );

		const int previousForwardCommand = d_->detachAxisDirection( AxisDirection( axis, true ) );
		if ( previousForwardCommand != 0 )
			emit bindChanged( previousForwardCommand );
	}

	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	FLAT_ASSERT(!commandInfo.isNull());

	d_->commandForAxis[ axis ] = command;
	commandInfo.boundAxes << axis;

	emit bindChanged( command );
}
#endif


void Context::detachButton(const Button button)
{
	const CommandId command = d_->detachButton(button);

	// TODO: emit bind
//	if ( command != 0 )
//		emit bindChanged( command );
}


#if 0
void Context::detachAxis( const Axis & axis )
{
	const int command = d_->detachAxis( axis );
	if ( command != 0 )
		emit bindChanged( command );
}


void Context::detachAxisDirection( const AxisDirection & axisDirection )
{
	const int command = d_->detachAxisDirection( axisDirection );
	if ( command != 0 )
		emit bindChanged( command );
}
#endif


void Context::detachCommand(const CommandId commandId)
{
#if 0
	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	FLAT_ASSERT(!commandInfo.isNull());

	foreach ( const Button & button, commandInfo.boundButtons )
		d_->commandForButton.remove( button );
	foreach ( const AxisDirection & axisDirection, commandInfo.boundAxisDirections )
		d_->commandForAxisDirection.remove( axisDirection );
	foreach ( const Axis & axis, commandInfo.boundAxes )
		d_->commandForAxis.remove( axis );

	commandInfo.boundButtons.clear();
	commandInfo.boundAxisDirections.clear();
	commandInfo.boundAxes.clear();

	emit bindChanged( command );
#endif
}


void Context::detachAll()
{
#if 0
	d_->commandForButton.clear();
	d_->commandForAxisDirection.clear();
	d_->commandForAxis.clear();

	for ( Tools::IdGeneratorIterator it( d_->manager->d_->commandIdGenerator ); it.hasNext(); )
	{
		ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ it.next() ];
		FLAT_ASSERT(!commandInfo.isNull());
		commandInfo.boundButtons.clear();
		commandInfo.boundAxisDirections.clear();
		commandInfo.boundAxes.clear();
	}

	for ( Tools::IdGeneratorIterator it( d_->manager->d_->commandIdGenerator ); it.hasNext(); )
		emit bindChanged( it.next() );
#endif
}


#if 0
QList<Button> Context::buttonsForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	FLAT_ASSERT(!commandInfo.isNull());
	return commandInfo.boundButtons;
}


QList<AxisDirection> Context::axisDirectionsForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	FLAT_ASSERT(!commandInfo.isNull());
	return commandInfo.boundAxisDirections;
}


QList<Axis> Context::axesForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	FLAT_ASSERT(!commandInfo.isNull());
	return commandInfo.boundAxes;
}
#endif




}}
