
#pragma once

#include <memory>

#include <Flat/Core/Ref.hpp>

#include "Common.hpp"




namespace Flat {
namespace Bind {




class Handler;




class FLAT_BIND_EXPORT Context
{
public:
	struct LockData;
	struct LockDeleter { void operator()(LockData * l) noexcept; };
	typedef std::unique_ptr<LockData, LockDeleter> Lock;

	~Context() noexcept;

	void attachButtonToCommand(Button button, CommandId commandId);
//	void attachAxisDirectionToCommand(AxisDirection axisDirection, CommandId commandId);
//	void attachAxisToCommand(Axis axis, CommandId commandId);

	void detachButton(Button button);
//	void detachAxis(Axis axis);
//	void detachAxisDirection(AxisDirection axisDirection);
	void detachCommand(CommandId commandId);
	void detachAll();

#if 0
	QList<Button> buttonsForCommand( int command ) const;
	QList<AxisDirection> axisDirectionsForCommand( int command ) const;
	QList<Axis> axesForCommand( int command ) const;
#endif

	Lock lock();

private:
	Context(ContextId contextId, Handler & handler) noexcept;

private:
	struct D;

	Ref<D> d_;

	friend class Handler;
};




}}
