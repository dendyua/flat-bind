
#include "Layout.hpp"
#include "Layout_p.hpp"




namespace Flat {
namespace Bind {




Layout::Layout(D & d) :
	d_(d)
{
}


Layout::~Layout()
{
}


Button Layout::button(const ButtonId buttonId) const noexcept
{
	return Button(d_->layoutId, buttonId);
}


Axis Layout::axis(const AxisId axisId) const noexcept
{
	return Axis(d_->layoutId, axisId);
}




}}
