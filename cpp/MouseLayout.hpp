
#pragma once

#include "Bind.h"




namespace Grim {
namespace Bind {




class MouseDevicePrivate;




class GRIM_BIND_EXPORT MouseDevice : public Device
{
	Q_OBJECT

public:
	enum ButtonType
	{
		Button_Null      = 0,
		Button_Left      = 1,
		Button_Right     = 2,
		Button_Middle    = 3,
		Button_WheelDown = 4,
		Button_WheelUp   = 5,
		Button_X1        = 6,
		Button_X2        = 7,

		Button_TotalButtons
	};

	enum AxisType
	{
		Axis_Null       = 0,
		Axis_Horizontal = 1,
		Axis_Vertical   = 2,

		Axis_TotalAxes
	};

	static ButtonType fromQtMouseButton( Qt::MouseButton buttonType );
	static ButtonType fromQtWheel( bool forward );
	static Qt::MouseButton toQtMouseButton( ButtonType buttonType );

	MouseDevice();
	~MouseDevice();

	// reimplemented from Device
	QString buttonToString( int button ) const;
	QString buttonName( int button ) const;

	QString axisToString( int axis ) const;
	QString axisName( int axis ) const;

private:
	const MouseDevicePrivate * constData() const;
	MouseDevicePrivate * data() const;
};




} // namespace Bind
} // namespace Grim
