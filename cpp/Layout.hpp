
#pragma once

#include <Flat/Core/Ref.hpp>
#include <Flat/Core/StringRef.hpp>

#include "Common.hpp"




namespace Flat {
namespace Bind {




class FLAT_BIND_EXPORT Layout
{
public:
	virtual ~Layout();

	Button button(ButtonId buttonId) const noexcept;
	Axis axis(AxisId axisId) const noexcept;

#if 0
	virtual StringRef buttonToString(int buttonId) const = 0;
	virtual StringRef axisToString(int axisId) const = 0;

	// TODO: Localization:
	//       - current names obtained from current localization setup
	//virtual StringRef buttonName(Localizator & l, int buttonId) const = 0;
	//virtual StringRef axisName(Localizator & l, int axisId) const = 0;
#endif

protected:
	struct D;

	Layout(D & d);

	Ref<D> d_;
};




}}
