
#pragma once

#include "Layout.hpp"
#include "Common.hpp"




namespace Flat {
namespace Bind {




struct Layout::D
{
	D(LayoutId layoutId) noexcept : layoutId(layoutId) {}
	virtual ~D() = default;

	const LayoutId layoutId;
};




}}
