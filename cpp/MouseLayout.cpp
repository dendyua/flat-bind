
#include "MouseDevice.h"
#include "Bind_p.h"




namespace Grim {
namespace Bind {




class MouseDevicePrivate : public DevicePrivate
{
public:
	void addButton( const int button, const QString & string, const char * const translationName );
	void addAxis( const int axis, const QString & name, const char * const translationName );

public:
	QHash<QString,int> buttonForString;
	QVector<ButtonInfo> buttonInfoForButton;

	QHash<QString,int> axisForString;
	QVector<AxisInfo> axisInfoForAxis;
};




void MouseDevicePrivate::addButton( const int button, const QString & string, const char * const translationName )
{
	buttonForString[ string ] = button;

	ButtonInfo buttonInfo;
	buttonInfo.button = button;
	buttonInfo.string = string;
	buttonInfo.translationName = translationName;
	buttonInfoForButton[ button ] = buttonInfo;
}


void MouseDevicePrivate::addAxis( const int axis, const QString & string, const char * const translationName )
{
	axisForString[ string ] = axis;

	AxisInfo axisInfo;
	axisInfo.axis = axis;
	axisInfo.string = string;
	axisInfo.translationName = translationName;
	axisInfoForAxis[ axis ] = axisInfo;
}




inline const MouseDevicePrivate * MouseDevice::constData() const
{ return constCastTo<MouseDevicePrivate>(); }

inline MouseDevicePrivate * MouseDevice::data() const
{ return castTo<MouseDevicePrivate>(); }




MouseDevice::ButtonType MouseDevice::fromQtMouseButton( const Qt::MouseButton buttonType )
{
	switch ( buttonType )
	{
	case Qt::LeftButton:
		return Button_Left;
	case Qt::RightButton:
		return Button_Right;
	case Qt::MiddleButton:
		return Button_Middle;
	case Qt::XButton1:
		return Button_X1;
	case Qt::XButton2:
		return Button_X2;
	}
	return Button_Null;
}


MouseDevice::ButtonType MouseDevice::fromQtWheel( const bool forward )
{
	return forward ? Button_WheelUp : Button_WheelDown;
}


Qt::MouseButton MouseDevice::toQtMouseButton( const ButtonType buttonType )
{
	switch ( buttonType )
	{
	case Button_Left:
		return Qt::LeftButton;
	case Button_Right:
		return Qt::RightButton;
	case Button_Middle:
		return Qt::MiddleButton;
	case Button_X1:
		return Qt::XButton1;
	case Button_X2:
		return Qt::XButton2;
	}
	return Qt::NoButton;
}




MouseDevice::MouseDevice() :
	Device( new MouseDevicePrivate )
{
	MouseDevicePrivate * const d = data();

	d->buttonInfoForButton.resize( Button_TotalButtons );
	d->addButton( Button_Left,      QLatin1String( "left" ),      QT_TR_NOOP("Left Button") );
	d->addButton( Button_Right,     QLatin1String( "right" ),     QT_TR_NOOP("Right Button") );
	d->addButton( Button_Middle,    QLatin1String( "middle" ),    QT_TR_NOOP("Middle Button") );
	d->addButton( Button_X1,        QLatin1String( "x1" ),        QT_TR_NOOP("X1 Button") );
	d->addButton( Button_X2,        QLatin1String( "x2" ),        QT_TR_NOOP("X2 Button") );
	d->addButton( Button_WheelDown, QLatin1String( "wheelDown" ), QT_TR_NOOP("Wheel Down") );
	d->addButton( Button_WheelUp,   QLatin1String( "wheelUp" ),   QT_TR_NOOP("Wheel Up") );

	d->axisInfoForAxis.resize( Axis_TotalAxes );
	d->addAxis( Axis_Horizontal, QLatin1String( "horizontal" ), QT_TR_NOOP("Horizontal") );
	d->addAxis( Axis_Vertical,   QLatin1String( "vertical" ),   QT_TR_NOOP("Vertical") );
}


MouseDevice::~MouseDevice()
{
}


QString MouseDevice::buttonToString( const int button ) const
{
	return constData()->buttonInfoForButton.at( button ).string;
}


QString MouseDevice::buttonName( const int button ) const
{
	return tr( constData()->buttonInfoForButton.at( button ).translationName );
}


QString MouseDevice::axisToString( const int axis ) const
{
	return constData()->axisInfoForAxis.at( axis ).string;
}


QString MouseDevice::axisName( const int axis ) const
{
	return tr( constData()->axisInfoForAxis.at( axis ).translationName );
}




} // namespace Bind
} // namespace Grim
