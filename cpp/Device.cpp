
#include "Device.hpp"

#include "Handler_p.hpp"




namespace Flat {
namespace Bind {




Device::Device(Handler & handler, const DeviceId deviceId) noexcept :
	handler_(handler),
	deviceId_(deviceId)
{
}


Device::~Device() noexcept
{
	handler_.d_->removeDevice(*this);
}


#if 0
void ManagerPrivate::handleAxis( CommandInfo & commandInfo, const qreal value )
{
	FLAT_ASSERT(commandInfo.isAxis);

	if ( commandInfo.value == value )
	{
		// value not changed, ignore
		return;
	}

	commandInfo.value = value;
	emit manager->axisCommandTriggered( commandInfo.command, value );
}
#endif


bool Device::handleButton(const Button button, const bool pressed)
{
	return handler_.d_->handleDeviceButton(deviceId_, button, pressed);
}


#if 0
bool Manager::handleAxis( const Axis & axis, const qreal value )
{
	foreach ( Context * const context, d_->currentContexts )
	{
		// check the axis direction first
		const int backwardCommand = context->d_->commandForAxisDirection.value( AxisDirection( axis, false ) );
		if ( backwardCommand != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ backwardCommand ];
			FLAT_ASSERT(!commandInfo.isNull());

			if ( commandInfo.isAxis )
			{
				// TODO: Implement button directions
			}
			else
			{
				d_->handleButton( commandInfo, value < 0 );
			}
		}

		const int forwardCommand = context->d_->commandForAxisDirection.value( AxisDirection( axis, true ) );
		if ( forwardCommand != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ forwardCommand ];
			FLAT_ASSERT(!commandInfo.isNull());

			if ( commandInfo.isAxis )
			{
				// TODO: Implement button directions
			}
			else
			{
				d_->handleButton( commandInfo, value > 0 );
			}
		}

		if ( backwardCommand != 0 || forwardCommand != 0 )
			return true;

		// check the regular axis
		const int command = context->d_->commandForAxis.value( axis );
		if ( command != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
			FLAT_ASSERT(!commandInfo.isNull());

			if ( !commandInfo.isAxis )
			{
				d_->handleButton( commandInfo, value != 0 );
			}
			else
			{
				d_->handleAxis( commandInfo, value );
			}
			return true;
		}
	}

	return false;
}
#endif




}}
