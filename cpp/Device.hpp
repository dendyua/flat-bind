
#pragma once

#include "Common.hpp"




namespace Flat {
namespace Bind {




class Handler;




class Device
{
public:
	~Device() noexcept;

	bool handleButton(Button button, bool pressed);
	bool handleAxis(Axis axis, float value);

private:
	Device(Handler & handler, DeviceId deviceId) noexcept;

	Handler & handler_;
	const DeviceId deviceId_;

	friend class Handler;
};




}}
