
#pragma once

#include "Bind.hpp"




namespace Flat {
namespace Bind {




class GenericDevicePrivate;




class FLAT_BIND_EXPORT GenericDevice : public Device
{
public:
	GenericDevice();
	~GenericDevice();

	// reimplemented from Device
	QString buttonToString( int button ) const;
	QString buttonName( int button ) const;

	QString axisToString( int axis ) const;
	QString axisName( int axis ) const;

private:
	const GenericDevicePrivate * constData() const;
	GenericDevicePrivate * data() const;
};




}}
