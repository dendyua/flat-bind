
#include "GenericDevice.hpp"
#include "Bind_p.hpp"




namespace Flat {
namespace Bind {




class GenericDevicePrivate : public DevicePrivate
{
public:
};




inline const GenericDevicePrivate * GenericDevice::constData() const
{ return constCastTo<GenericDevicePrivate>(); }

inline GenericDevicePrivate * GenericDevice::data() const
{ return castTo<GenericDevicePrivate>(); }




GenericDevice::GenericDevice() :
	Device( new GenericDevicePrivate )
{
}


GenericDevice::~GenericDevice()
{
}


QString GenericDevice::buttonToString( const int button ) const
{
	static const QString kButtonStringTemplate = QLatin1String( "button%1" );
	return kButtonStringTemplate.arg( button );
}


QString GenericDevice::buttonName( const int button ) const
{
	return tr( "Button %1" ).arg( button );
}


QString GenericDevice::axisToString( const int axis ) const
{
	static const QString kAxisStringTemplate = QLatin1String( "axis%1" );
	return kAxisStringTemplate.arg( axis );
}


QString GenericDevice::axisName( const int axis ) const
{
	return tr( "Axis %1" ).arg( axis );
}




}}
