
#pragma once

#include "Context.hpp"




namespace Flat {
namespace Bind {




struct Context::D
{
	struct CommandInfo
	{
		std::vector<Button> boundButtons;
//		std::vector<Axis> boundAxes;
//		std::vector<AxisDirection> boundAxisDirections;
	};

	D(ContextId contextId, Context & context, Handler & handler) noexcept :
		contextId(contextId),
		context(context),
		handler(handler)
	{}

	void lock();
	void unlock();

	CommandId detachButton(Button button) noexcept;
	CommandId detachAxis(Axis axis) noexcept;
//	int detachAxisDirection( const AxisDirection & axisDirection );

	const ContextId contextId;
	Context & context;
	Handler & handler;

	std::vector<CommandInfo> commandInfos;

	std::unordered_map<Button, CommandId> commandIdForButton;
	std::unordered_map<Axis, CommandId> commandIdForAxis;
//	std::unordered_map<AxisDirection, int> commandForAxisDirection;

	int lockCount_ = 0;
};




}}
