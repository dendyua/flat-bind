
#pragma once

#include "Bind.hpp"

#include <Flat/Utils/IdGenerator.h>




namespace Flat {
namespace Bind {












inline DevicePrivate::DevicePrivate() :
	type_( 0 )
{}

inline int DevicePrivate::type() const
{ return type_; }




}}
